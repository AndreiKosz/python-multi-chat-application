import rpyc
from my_client import MyClient

from tkinter import *


class clientGUI(Frame):
    def __init__(self, master=None):
        self.client = None
        super().__init__(master)
        self.varName = StringVar()
        self.frameClient = Frame(self.master)
        self.frameClient.grid(row=0, column=0)
        self.listClients = Listbox(self.master, height=20, width=30, selectmode=MULTIPLE)
        self.listClients.grid(row=3, column=1)
        self.nameLabel = Label(self.frameClient, text="name")
        self.nameLabel.grid(row=0, column=0)
        self.nameEntry = Entry(self.frameClient)
        self.nameEntry.grid(row=0, column=1)
        self.nameEntry.insert(0, "unknown")
        self.hostLabel = Label(self.frameClient, text="host")
        self.hostLabel.grid(row=1, column=0)
        self.hostEntry = Entry(self.frameClient)
        self.hostEntry.grid(row=1, column=1)
        self.hostEntry.insert(0, "localhost")
        self.portLabel = Label(self.frameClient, text="port")
        self.portLabel.grid(row=2, column=0)
        self.portEntry = Entry(self.frameClient)
        self.portEntry.grid(row=2, column=1)
        self.portEntry.insert(0, "18861")
        self.connectButton = Button(self.frameClient, text="Connect", command=self.connect)
        self.connectButton.grid(columnspan=2)
        self.messageText = Text(self.master, height=20, width=39)
        self.messageText.grid(row=3, column=0)
        self.exntryText = Entry(self.master, width=52)
        self.exntryText.grid(rowspan=2)
        self.sendButton = Button(self.master, text="send", padx=25, command=self.send)
        self.sendButton.grid(row=4, column=1)
        self.sendAllButton = Button(self.master, text="send to all", padx=25, command=self.sendAll)
        self.sendAllButton.grid(row=5, column=1)
        self.master.title("ClientChat")

    def sendAll(self):
        text = self.exntryText.get()
        try:
            self.client.send_all(text)
        except:
            pass

    def send(self):
        client = self.listClients.curselection()

        text = self.exntryText.get()
        try:
            self.client.send(self.client.name, text)
            for i in client:
                self.client.send(self.listClients.get(i), text)

            self.exntryText.delete(0, len(text))
        except:
            pass

    def connect(self):
        try:
            name = self.nameEntry.get()
            host = self.hostEntry.get()
            port = self.portEntry.get()
            self.client = MyClient(name, host, int(port), self.add_clients, self.del_clients, self.messageText.insert)
            conn = self.client.connect()
            if conn:
                self.officialNume = Label(self.master, text="your name:\n%s" % (name,))
                self.officialNume.grid(row=0, column=1)
                self.nameEntry.delete(0, len(name))
                self.hostEntry.delete(0, len(host))
                self.portEntry.delete(0, len(port))
        except ValueError:
            pass

    def add_clients(self, name):
        self.listClients.insert(END, name)

    def del_clients(self, name):
        for i in range(0, self.listClients.size()):
            if self.listClients.get(i) == name:
                self.listClients.delete(i)


def start_GUI():
    t = Tk()
    c = clientGUI(t)
    c.mainloop()


if __name__ == "__main__":
    start_GUI()
