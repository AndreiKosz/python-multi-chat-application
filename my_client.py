import threading
import rpyc
import sys
from tkinter import *


class MyClient:
    def __init__(self, name, host="localhost", port=18861, callback=None, callback_out=None, print_callback=None):
        self.name = name
        self.conn = None
        self.token = None
        self.active = True
        self.host = host
        self.port = port
        self.callback = callback
        self.callback_out = callback_out
        self.print_callback = print_callback
        if print_callback is None:
            self.t_work = threading.Thread(target=self.work_cli)
        else:
            self.t_work = threading.Thread(target=self.work)
        self.t_work.setDaemon(True)

    def connect(self):
        try:
            self.conn = rpyc.connect(self.host, self.port)
        except Exception:
            print("Conection failed")
            return
        try:
            self.token = self.conn.root.log_in(self.name, self.callback, self.callback_out)
            self.t_work.start()
            return True
        except ValueError:
            raise ValueError("Already Connected")
        return False

    def send(self, destin, messg):
        self.token.say_mesg(dest=destin, mesg=messg + "\n")





    def send_all(self, messg):
        self.token.exposed_say_mesg_all(mesg=messg + "\n")


    def get_clients(self):
        return self.token.get_clients()

    def work_cli(self):
        while self.active:
            try:
                if self.token.is_ready():
                    print(self.token.get_mesg())
                    self.token.change()
            except:
                print("An error has occur2!!!")
                break

    def work(self):
        while self.active:
            try:
                if self.token.is_ready():
                    self.print_callback(END, self.token.get_mesg())
                    self.token.change()
            except:
                print("An error has occur!!!")
                break
