import rpyc
from rpyc.utils import server
from rpyc import async
import threading
import sys


class CustomServer(server.Server):
    def _accept_method(self, sock):
        t = threading.Thread(target=self._authenticate_and_serve_client, args=(sock,))
        t.setDaemon(True)
        t.start()


tokens = set()


class ChatToken:
    def __init__(self, name, callback=None, callback_out=None):
        self.name = name
        self.mesg = ""
        self.ready = False
        self.callback = callback
        self.callback_out = callback_out
        global tokens
        if not callback is None:
            for tok in tokens:
                if not tok.callback is None:
                    tok.callback(name)
                self.callback(tok.name)
        tokens.add(self)

    def exposed_change(self):
        self.ready = False
        self.mesg = ""

    def exposed_say_mesg(self, dest="", mesg=""):
        print("say %s to %s" % (self.name, dest,))
        global tokens
        for tok in tokens:
            if tok.name == dest:
                while tok.ready:
                    pass
                tok.mesg += "[%s]->%s" % (self.name, mesg,)
                tok.ready = True

    def log_out(self):
        global tokens
        print("log_out")
        tokens.remove(self)
        for tok in tokens:
            if not tok.callback_out is None:
                tok.callback_out(self.name)

    def exposed_is_ready(self):
        return self.ready

    def exposed_get_clients(self):
        global tokens
        clienti = set()
        for token in tokens:
            clienti.add(token.name)
        return clienti

    def exposed_say_mesg_all(self, mesg=""):
        print("say %s to all" % self.name)
        global tokens
        for tok in tokens:
            while tok.ready:
                pass
            tok.mesg += "[%s]->%s" % (self.name, mesg,)
            tok.ready = True

    def exposed_get_mesg(self):
        return self.mesg


class ChatService(rpyc.Service):
    ALIASES = ["Chat", "ClientChat"]

    def on_connect(self):
        self.token = None
        print("connected")

    def exposed_log_in(self, name, callback, callback_out):
        global tokens
        # print(tokens)
        for token in tokens:
            if token.name == name:
                raise ValueError("%s exista!".format(name))
        self.token = ChatToken(name, callback, callback_out)
        print("logged in %s" % self.token.name)
        return self.token

    def exposed_log_out(self):
        print("logg")
        self.token.log_out()
        self.token = None

    def on_disconnect(self):
        if not self.token is None:
            print("disconnected %s" % (self.token.name))
            self.exposed_log_out()


if __name__ == "__main__":
    t = CustomServer(ChatService, port=18861)
    t.start()
